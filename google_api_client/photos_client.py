"""Reporting Analytic API V4."""

from django.conf import settings
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import reverse

from apiclient.discovery import build
import google_auth_oauthlib.flow

import httplib2


SCOPES = ['https://www.googleapis.com/auth/photoslibrary.readonly']


def get_oauth_url(request=None):
    """
    TODO
    """
    protocol = 'http'

    if request:
        if request.is_secure():
            protocol = 'https'
        domain = get_current_site(request).domain
    else:
        current_site = Site.objects.get_current()
        domain = current_site.domain

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        settings.GOOGLE_KEY_FILE_LOCATION,
        scopes=SCOPES,
    )

    flow.redirect_uri = '{}://{}{}'.format(
        protocol,
        domain,
        reverse('google_auth2_callback')
    )

    return flow.authorization_url()


def get_auth_token(code, state, request):
    """
    TODO
    """
    protocol = 'http'

    if request:
        if request.is_secure():
            protocol = 'https'
        domain = get_current_site(request).domain
    else:
        current_site = Site.objects.get_current()
        domain = current_site.domain

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        settings.GOOGLE_KEY_FILE_LOCATION,
        scopes=SCOPES,
    )

    flow.redirect_uri = '{}://{}{}'.format(
        protocol,
        domain,
        reverse('google_auth2_callback')
    )

    return flow.fetch_token(code=code)


def whut(self):
    credentials = ServiceAccountCredentials.from_p12_keyfile(
        SERVICE_ACCOUNT_EMAIL, KEY_FILE_LOCATION, scopes=SCOPES)

    http = credentials.authorize(httplib2.Http())

    # Build the service object.
    analytics = build(
        'photoslibrary',
        'v1',
        http=http,
        discoveryServiceUrl=DISCOVERY_URI
    )

    return analytics


