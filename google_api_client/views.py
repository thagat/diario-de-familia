# -*- coding: utf-8 -*-
""" Views for the google_api_client application. """
# standard library

# django

# models

# views
from base.views import BaseRedirectView

# other
from google_api_client.photos_client import get_auth_token
from google_api_client.photos_client import get_oauth_url


class GoogleLogin(BaseRedirectView):
    """
    """

    def get_redirect_url(self, *args, **kwargs):
        oauth_url = get_oauth_url(self.request)
        return oauth_url[0]


class GoogleOauth2Callback(BaseRedirectView):
    """
    """

    def get(self, request):
        code = request.GET.get('code')
        state = request.GET.get('state')
        get_auth_token(code, state, self.request)
