from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^oauth2callback/$',
        views.GoogleOauth2Callback.as_view(),
        name='google_auth2_callback'
    ),
    url(
        r'^login/$',
        views.GoogleLogin.as_view(),
        name='google_login'
    ),
]
