# -*- coding: utf-8 -*-
""" Administration classes for the memories application. """
# standard library

# django
from django.contrib import admin

# models
from .models import Memory
from .models import MemoryImage

# other
from import_export import resources
from import_export.admin import ImportExportActionModelAdmin


class MemoryResource(resources.ModelResource):

    class Meta:
        model = Memory
        fields = (
            'id',
            'user',
            'text',
            'to_date',
        )


@admin.register(Memory)
class MemoryAdmin(ImportExportActionModelAdmin):
    resource_class = MemoryResource
    list_display = (
        'from_date',
        'to_date',
        'user',
        'text',
    )


@admin.register(MemoryImage)
class MemoryImageAdmin(ImportExportActionModelAdmin):
    list_display = (
        'memory',
        'src',
    )

    list_filter = ('memory__to_date',)
