from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^$',
        views.MemoryListView.as_view(),
        name='memory_list'
    ),
    url(
        r'^(?P<date>\d\d\d\d-\d\d-\d\d)/$',
        views.MemoryDateListView.as_view(),
        name='memory_list'
    ),
    url(
        r'^create/$',
        views.MemoryCreateView.as_view(),
        name='memory_create'
    ),
    url(
        r'^(?P<pk>[\d]+)/$',
        views.MemoryDetailView.as_view(),
        name='memory_detail'
    ),
    url(
        r'^(?P<pk>[\d]+)/update/$',
        views.MemoryUpdateView.as_view(),
        name='memory_update'
    ),
    url(
        r'^(?P<pk>[\d]+)/delete/$',
        views.MemoryDeleteView.as_view(),
        name='memory_delete',
    ),
    url(
        r'^(?P<memory_id>[\d]+)/images/create/$',
        views.MemoryImageCreateView.as_view(),
        name='memory_image_create'
    ),
]
