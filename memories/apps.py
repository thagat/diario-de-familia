from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MemoriesConfig(AppConfig):
    name = 'memories'
    verbose_name = _('memories')
