# -*- coding: utf-8 -*-
""" Views for the memories application. """
# standard library

# django
from django.http import JsonResponse

# models
from .models import Memory
from .models import MemoryImage
from tasks.models import Task

# views
from base.views import BaseCreateView
from base.views import BaseDeleteView
from base.views import BaseDetailView
from base.views import BaseListView
from base.views import BaseSubModelCreateView
from base.views import BaseUpdateView

# forms
from .forms import MemoryForm
from .forms import MemoryAdminForm
from .forms import MemoryImageForm

# base
from base.serializers import ModelEncoder


class MemoryListView(BaseListView):
    """
    View for displaying a list of memories.
    """
    model = Memory
    template_name = 'memories/memory_list.pug'
    permission_required = 'memories.view_memory'
    ordering = '-to_date'

    def get_title(self):
        return 'Diario de familia'

    def get_queryset(self):
        queryset = super().get_queryset()

        if 'q' in self.request.GET:
            query = self.request.GET['q']
            if query:
                queryset = queryset.search(query)

        queryset = queryset.select_related('image')

        return queryset


class MemoryDateListView(BaseListView):
    """
    View for displaying a list of memories.
    """
    model = Memory
    template_name = 'memories/memory_date_list.pug'
    permission_required = 'memories.view_memory'
    ordering = '-to_date'

    def get_title(self):
        return self.kwargs['date']

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(to_date=self.kwargs['date'])
        queryset = queryset.select_related('image')

        return queryset


class MemoryCreateView(BaseCreateView):
    """
    A view for creating a single memory
    """
    model = Memory
    form_class = MemoryForm
    template_name = 'memories/memory_create.pug'
    permission_required = 'memories.add_memory'

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = Memory(
            user=self.request.user,
            to_date=self.request.GET.get('to_date')
        )
        return kwargs

    def get_form_class(self):
        if self.request.user.is_superuser:
            return MemoryAdminForm

        return super().get_form_class()


class MemoryDetailView(BaseDetailView):
    """
    A view for displaying a single memory
    """
    model = Memory
    template_name = 'memories/memory_detail.pug'
    permission_required = 'memories.view_memory'

    def get_title(self):
        return self.object.to_date

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        tasks = Task.objects.filter(date=self.object.to_date).select_related(
            'task_template'
        )
        context['tasks'] = tasks
        return context


class MemoryUpdateView(BaseUpdateView):
    """
    A view for editing a single memory
    """
    model = Memory
    form_class = MemoryForm
    template_name = 'memories/memory_update.pug'
    permission_required = 'memories.change_memory'


class MemoryDeleteView(BaseDeleteView):
    """
    A view for deleting a single memory
    """
    model = Memory
    permission_required = 'memories.delete_memory'
    template_name = 'memories/memory_delete.pug'


class MemoryImageCreateView(BaseSubModelCreateView):
    """
    A view for creating a single memory
    """
    model = MemoryImage
    form_class = MemoryImageForm
    template_name = 'memories/memory_create.pug'
    permission_required = 'memories.add_memory'
    parent_model = Memory

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs['instance'] = MemoryImage(memory_id=self.kwargs['memory_id'])
        return kwargs

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return JsonResponse(self.object.to_dict(), encoder=ModelEncoder)
