# -*- coding: utf-8 -*-
""" Models for the memories application. """
# standard library
import datetime

# django
from django.urls import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from base.models import BaseModel
from users.models import User
from .managers import MemoryQuerySet
from easy_thumbnails.fields import ThumbnailerImageField

# base
from base import utils
from base.models import file_path


class Memory(BaseModel):
    """
    Model and store memories from users
    """
    # foreign keys
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('user'),
        related_name='memories',
    )
    image = models.ForeignKey(
        'MemoryImage',
        on_delete=models.SET_NULL,
        verbose_name=_('image'),
        related_name='audios',
        null=True,
        blank=True,
    )
    # required fields
    text = models.TextField(
        _('text'),
    )
    from_date = models.DateField(
        editable=False,
    )
    to_date = models.DateField(
        default=utils.today,
    )
    # optional fields

    # Manager
    objects = MemoryQuerySet.as_manager()

    class Meta:
        verbose_name = _('memory')
        verbose_name_plural = _('memories')
        ordering = ('to_date',)

    def __str__(self):
        return '{}: {}'.format(self.user.first_name, self.to_date)

    # custom methods
    def next(self):
        return Memory.objects.filter(
            to_date__gte=self.to_date,
        ).exclude(id=self.id).order_by('to_date').first()

    def next_date(self):
        return self.to_date + datetime.timedelta(1)

    def previous(self):
        return Memory.objects.filter(
            to_date__lte=self.to_date,
        ).exclude(id=self.id).order_by('to_date').last()

    def previous_date(self):
        return self.to_date - datetime.timedelta(1)

    # django methods
    def get_absolute_url(self):
        """ Returns the canonical URL for the Memory object """
        return reverse('memory_detail', args=(self.pk,))

    def save(self, *args, **kwargs):
        if not self.from_date:
            self.from_date = self.to_date

        super().save(*args, **kwargs)


class MemoryImage(BaseModel):
    # foreign keys
    memory = models.ForeignKey(
        Memory,
        on_delete=models.CASCADE,
        verbose_name=_('memory'),
        related_name='images',
    )
    # required fields
    src = ThumbnailerImageField(
        upload_to=file_path,
        max_length=255,
    )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if self.memory.image_id is None:
            self.memory.update(image=self)
