from base.managers import QuerySet


class MemoryQuerySet(QuerySet):

    def search(self, query):
        """
        Search Memory objects by name
        """
        if query:
            return self.filter(
                text__unaccent__icontains=query
            )
