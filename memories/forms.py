# -*- coding: utf-8 -*-
""" Forms for the memories application. """
# standard library

# django

# models
from .models import Memory
from .models import MemoryImage

# views
from base.forms import BaseModelForm


class MemoryForm(BaseModelForm):
    """
    Form Memory model.
    """

    class Meta:
        model = Memory
        fields = ('to_date', 'text')


class MemoryAdminForm(BaseModelForm):
    """
    Form Memory model.
    """

    class Meta:
        model = Memory
        fields = ('to_date', 'user', 'text')


class MemoryImageForm(BaseModelForm):
    """
    Form MemoryImage model.
    """

    class Meta:
        model = MemoryImage
        fields = ('src',)
