"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.urls import include, path
from django.contrib import admin
from django.conf.urls.static import static

from base import views as base_views
from memories import views as memory_views

urlpatterns = [
    url(r'^admin/', include('loginas.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('users.urls')),
    url(r'^regions/', include('regions.urls')),
    url(r'^status/$', base_views.StatusView.as_view(), name='status'),
    url(r'^api/v1/', include('api.urls', namespace='api')),
    url(r'^memories/', include('memories.urls')),
    url(r'^tasks/', include('tasks.urls')),
    url(r'^google/', include('google_api_client.urls')),
    url(
        r'^privacy-policy/$',
        base_views.PrivacyPolicy.as_view(),
        name='privacy_policy'
    ),
    url(
        r'^terms/$',
        base_views.TermsOfService.as_view(),
        name='terms_of_service'
    ),
    url(r'^$', memory_views.MemoryListView.as_view(), name='home'),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns + static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT,
    )

# custom error pages
handler400 = 'base.views.bad_request_view'
handler403 = 'base.views.permission_denied_view'
handler404 = 'base.views.page_not_found_view'
handler500 = 'base.views.server_error_view'
