# -*- coding: utf-8 -*-
""" Administration classes for the tasks application. """
# standard library

# django
from django.contrib import admin

# models
from .models import Task
from .models import TaskTemplate


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'task_template', 'comment')


@admin.register(TaskTemplate)
class TaskTemplateAdmin(admin.ModelAdmin):
    pass
