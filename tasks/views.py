# -*- coding: utf-8 -*-
""" Views for the tasks application. """
# standard library
import datetime

# django
from django.shortcuts import redirect

# models
from .models import Task
from .models import TaskTemplate

# views
from base.views import BaseCreateView
from base.views import BaseDeleteView
from base.views import BaseDetailView
from base.views import BaseListView
from base.views import BaseUpdateView

# forms
from .forms import TaskForm
from .forms import TaskTemplateForm

# base
from base import utils


class TaskListView(BaseListView):
    """
    View for displaying a list of tasks.
    """
    model = Task
    template_name = 'tasks/task_list.pug'
    permission_required = 'tasks.view_task'


class TaskCreateView(BaseCreateView):
    """
    A view for creating a single task
    """
    model = Task
    form_class = TaskForm
    template_name = 'tasks/task_create.pug'
    permission_required = 'tasks.add_task'

    def get_initial(self):
        initial = super().get_initial()

        if 'date' in self.kwargs:
            date_str = self.kwargs['date']
            date = datetime.datetime.strptime(date_str, '%Y-%m-%d').date()
            initial['date'] = date

        initial['users'] = [self.request.user]

        return initial

    def form_valid(self, form):
        task_template = form.cleaned_data['task_template']
        memory = task_template.create_task(
            date=form.cleaned_data['date'],
            users=form.cleaned_data['users'],
            comment=form.cleaned_data['comment'],
        )
        return redirect(memory.get_absolute_url())


class TaskDetailView(BaseDetailView):
    """
    A view for displaying a single task
    """
    model = Task
    template_name = 'tasks/task_detail.pug'
    permission_required = 'tasks.view_task'


class TaskUpdateView(BaseUpdateView):
    """
    A view for editing a single task
    """
    model = Task
    form_class = TaskForm
    template_name = 'tasks/task_update.pug'
    permission_required = 'tasks.change_task'


class TaskDeleteView(BaseDeleteView):
    """
    A view for deleting a single task
    """
    model = Task
    permission_required = 'tasks.delete_task'
    template_name = 'tasks/task_delete.pug'


class TaskTemplateListView(BaseListView):
    """
    View for displaying a list of tasks.
    """
    model = TaskTemplate
    template_name = 'tasks/task_template_list.pug'
    permission_required = 'tasks.view_tasktemplate'
    paginate_by = None

    def get_queryset(self):
        return super().get_queryset().filter(task_template=None)


class TaskTemplateCreateView(BaseCreateView):
    """
    A view for creating a single task_template
    """
    model = TaskTemplate
    form_class = TaskTemplateForm
    template_name = 'tasks/task_template_create.pug'
    permission_required = 'tasks.add_tasktemplate'


class TaskTemplateDetailView(BaseDetailView):
    """
    A view for displaying a single task_template
    """
    model = TaskTemplate
    template_name = 'tasks/task_template_detail.pug'
    permission_required = 'tasks.view_tasktemplate'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.children.exists():
            memory = self.object.create_task(
                date=utils.today(),
                users=[self.request.user],
                comment=''
            )
            return redirect(memory.get_absolute_url())
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class TaskTemplateUpdateView(BaseUpdateView):
    """
    A view for editing a single task_template
    """
    model = TaskTemplate
    form_class = TaskTemplateForm
    template_name = 'tasks/task_template_update.pug'
    permission_required = 'tasks.change_tasktemplate'


class TaskTemplateDeleteView(BaseDeleteView):
    """
    A view for deleting a single task_template
    """
    model = TaskTemplate
    permission_required = 'tasks.delete_tasktemplate'
    template_name = 'tasks/task_template_delete.pug'
