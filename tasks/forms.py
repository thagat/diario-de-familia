# -*- coding: utf-8 -*-
""" Forms for the tasks application. """
# standard library

# django
# from django import forms

# models
from .models import Task
from .models import TaskTemplate

# views
from base.forms import BaseModelForm


class TaskForm(BaseModelForm):
    """
    Form Task model.
    """

    class Meta:
        model = Task
        exclude = ()


class TaskTemplateForm(BaseModelForm):
    """
    Form TaskTemplate model.
    """

    class Meta:
        model = TaskTemplate
        exclude = ()
