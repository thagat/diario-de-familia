from django.urls import include, path

from . import views


task_urlpatterns = [
    path(
        '',
        views.TaskListView.as_view(),
        name='task_list'
    ),
    path(
        'create/',
        views.TaskCreateView.as_view(),
        name='task_create'
    ),
    path(
        '<int:pk>/',
        views.TaskDetailView.as_view(),
        name='task_detail'
    ),
    path(
        'create/date/<date>/',
        views.TaskCreateView.as_view(),
        name='task_create'
    ),
    path(
        '<int:pk>/update/',
        views.TaskUpdateView.as_view(),
        name='task_update'
    ),
    path(
        '<int:pk>/delete/',
        views.TaskDeleteView.as_view(),
        name='task_delete',
    ),
]

task_template_urlpatterns = [
    path(
        '',
        views.TaskTemplateListView.as_view(),
        name='tasktemplate_list'
    ),
    path(
        'create/',
        views.TaskTemplateCreateView.as_view(),
        name='tasktemplate_create'
    ),
    path(
        '<int:pk>/',
        views.TaskTemplateDetailView.as_view(),
        name='tasktemplate_detail'
    ),
    path(
        '<int:pk>/update/',
        views.TaskTemplateUpdateView.as_view(),
        name='tasktemplate_update'
    ),
    path(
        '<int:pk>/delete/',
        views.TaskTemplateDeleteView.as_view(),
        name='tasktemplate_delete',
    ),
]

urlpatterns = [
    path('', include(task_urlpatterns)),
    path('templates/', include(task_template_urlpatterns)),
]
