# -*- coding: utf-8 -*-
""" Models for the tasks application. """
# standard library
import random

# django
from django.urls import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from .managers import TaskQuerySet
from base.models import BaseModel
from colorfield.fields import ColorField
from fontawesome_5.fields import IconField
from memories.models import Memory
from users.models import User


def random_color():
    return '#' + '{0:#0x}'.format(random.randint(0, 16777215))[2:]


class TaskTemplate(BaseModel):
    """
    """
    # foreign keys
    task_template = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name=_('template'),
        null=True,
        blank=True,
        related_name='children',
    )
    # required fields
    name = models.CharField(
        _('name'),
        max_length=30,
        unique=True,
    )
    color = ColorField(
        default=random_color
    )
    icon = IconField()

    # optional fields
    def get_absolute_url(self):
        """ Returns the canonical URL for the Task object """
        return reverse('tasktemplate_detail', args=(self.pk,))

    def create_task(self, date, users, comment):
        user = users[0]

        memory = Memory.objects.get_or_create(
            to_date=date,
            defaults={
                'text': comment,
                'user': user,
            }
        )[0]

        task = self.tasks.update_or_create(
            task_template=self,
            date=date,
            defaults={
                'comment': comment,
            }
        )[0]
        task.users.add(*(u.id for u in users))

        return memory

    def __str__(self):
        return self.name


class Task(BaseModel):
    """
    TODO: Fill this description
    The tasks system is used to store task.
    """
    # foreign keys
    task_template = models.ForeignKey(
        TaskTemplate,
        on_delete=models.CASCADE,
        verbose_name=_('template'),
        related_name='tasks',
    )
    users = models.ManyToManyField(
        User,
        verbose_name=_('user'),
        related_name='tasks',
    )
    # required fields
    date = models.DateField(
        _('date'),
    )
    # optional fields
    comment = models.CharField(
        _('comment'),
        max_length=255,
        blank=True,
        default='',
    )

    # Manager
    objects = TaskQuerySet.as_manager()

    class Meta:
        verbose_name = _('task')
        verbose_name_plural = _('tasks')
        unique_together = ('task_template', 'date')

    def __str__(self):
        return self.comment or self.task_template.name

    def get_absolute_url(self):
        """ Returns the canonical URL for the Task object """
        return reverse('task_detail', args=(self.pk,))
