# Generated by Django 2.2.3 on 2020-12-07 13:53

import colorfield.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import fontawesome_5.fields
import tasks.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('name', models.CharField(max_length=30, unique=True, verbose_name='name')),
                ('color', colorfield.fields.ColorField(default=tasks.models.random_color, max_length=18)),
                ('icon', fontawesome_5.fields.IconField(blank=True, max_length=60)),
                ('task_template', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='tasks.TaskTemplate', verbose_name='template')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='creation date', verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='edition date', null=True, verbose_name='updated at')),
                ('date', models.DateField(verbose_name='date')),
                ('comment', models.CharField(blank=True, default='', max_length=255, verbose_name='comment')),
                ('task_template', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to='tasks.TaskTemplate', verbose_name='template')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name': 'task',
                'verbose_name_plural': 'tasks',
            },
        ),
    ]
