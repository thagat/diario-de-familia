from base.managers import QuerySet


class TaskQuerySet(QuerySet):

    def search(self, query):
        """
        Search Task objects by name
        """
        if query:
            # TODO implement this method, since this is an example
            return self.filter(
                name__unaccent__icontains=query
            )
